package com.mrunia.smartlock;

import android.bluetooth.BluetoothGatt;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import nl.littlerobots.bean.Bean;
import nl.littlerobots.bean.BeanDiscoveryListener;
import nl.littlerobots.bean.BeanListener;
import nl.littlerobots.bean.BeanManager;
import nl.littlerobots.bean.internal.ble.GattClient;
import nl.littlerobots.bean.message.Callback;
import nl.littlerobots.bean.message.RadioConfig;
import nl.littlerobots.bean.message.ScratchData;
import nl.littlerobots.bean.message.SketchMetaData;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";

    private static final byte DISCONNECTED[] = {0x00};
    private static final byte LOWSIGNAL[] = {0x01};
    private static final byte HIGHSIGNAL[] = {0x02};

    // create a listener
    BeanDiscoveryListener beanDiscoveryListener;
    ArrayList<Bean> beanList;
    BeanListAdapter beanListAdapter;
    ListView beanListView;
    TextView dataDisplay;
    BeanListener myBeanListener;
    Bean selectedBean;
    Button open, close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        beanListView = (ListView) findViewById(R.id.beanList);
        dataDisplay = (TextView) findViewById(R.id.data);
        open = (Button) findViewById(R.id.open);
        close = (Button) findViewById(R.id.close);
        beanList = new ArrayList<Bean>();
        beanListAdapter = new BeanListAdapter(this, android.R.layout.simple_list_item_multiple_choice, beanList);
        beanListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        beanListView.setAdapter(beanListAdapter);
        beanDiscoveryListener = new BeanDiscoveryListener() {
            @Override
            public void onBeanDiscovered(Bean bean) {
                beanList.add(bean);
                beanListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDiscoveryComplete() {
                //TODO: do nothing for now.
            }
        };
        myBeanListener = new BeanListener() {
            @Override
            public void onConnected() {
                dataDisplay.setText("You are now connected!");
                open.setClickable(true);
                close.setClickable(true);
            }

            @Override
            public void onConnectionFailed() {
                dataDisplay.setText("CONNECTION FAILED!");
            }

            @Override
            public void onDisconnected() {
                //called when disconnects happen unexpectedly
                dataDisplay.setText("You have been DISCONNECTED!");
            }

            @Override
            public void onSerialMessageReceived(byte[] bytes) {
                StringBuilder b = new StringBuilder();
                for(int i=0; i<bytes.length; i++) {
                    b.append(bytes[i]);
                }
                Log.i(TAG, "\n\n"+b.toString()+"\n\n");
            }

            @Override
            public void onScratchValueChanged(int i, byte[] bytes) {
                Log.i(TAG, "SCRATCHDATA WAS CHANGED IN BANK: "+i+" TO "+bytes[0]);
                selectedBean.disconnect();
            }
        };

        // Assuming "this" is an activity or service:
        BeanManager.getInstance().startDiscovery(beanDiscoveryListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void connect(View v) {
        int position = beanListAdapter.checkedBean;
        if(position >= 0) {
            selectedBean = beanList.get(position);
            selectedBean.connect(this, myBeanListener);
        } else {
            Toast.makeText(this, "No devices were selected.", Toast.LENGTH_LONG).show();
        }
    }

    public void open(View v) {
        selectedBean.setScratchData(Bean.SCRATCH_BANK_1 , HIGHSIGNAL);
    }

    public void close(View v) {
        selectedBean.setScratchData(Bean.SCRATCH_BANK_1 , LOWSIGNAL);
    }
}

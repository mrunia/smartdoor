package com.mrunia.smartlock;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import nl.littlerobots.bean.Bean;

/**
 * Created by mar on 1/16/15.
 */
public class BeanListAdapter extends ArrayAdapter<Bean> {
    private static final String TAG = "BeanListAdapter";
    private Activity activity;
    private List<Bean> beanList;
    private LayoutInflater inflater;
    public int checkedBean = -1;

    public BeanListAdapter(Activity activity, int resource, List<Bean> beanList) {
        super(activity, resource, beanList);
        this.activity = activity;
        this.beanList = beanList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.bean_list, parent, false);
        }

        Bean currBean = beanList.get(position);
        TextView beanName = (TextView) convertView.findViewById(R.id.beanName);
        beanName.setText(currBean.getDevice().getName());

        CheckBox beanCheckbox = (CheckBox) convertView.findViewById(R.id.beanCheckbox);
        beanCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) checkedBean = position;
            }
        });

        return convertView;
    }
}


/*
* Created by: Max Runia
* Updated on: Jan 19, 2015
* Hardware LightBlueBean: https://punchthrough.com/bean/
* Lock construction: https://punchthrough.com/bean/examples/smartphone-controlled-lock/
* Sketch part of the SmartDoor project: https://bitbucket.org/mrunia/smartdoor/overview
*/

#define ACCELTHRESHOLD 100 // When acceleration change goes beyond this threshold, the door was opened.ß
#define UNLOCK_TIMEOUT_MS 300
#define LOCK_TIMEOUT_MS 275

/********************************* STATES OF DOOR / LOCK *****************************************/
const int OFF         = 0; //bean goes into resting state, no connection.
const int ONCLOSED    = 1; //bluetooth device connects but not strong enough to unlock door yet.
const int ONOPENNOMOVE= 2; //lock is open, not moving.
const int ONOPENMOVING= 3; //lock is open, door is moving.

/********************************* MOTOR CONTROLLER VARS *****************************************/
const int PWMA = 1; //Speed control 
const int AIN1 = 0; //Direction
const int AIN2 = 2; //Direction
const int STBY = 3; //Standby
const int SWCH = 5; //Switch

/********************************* SIGNAL STRENGTH VARS ******************************************/
const uint8_t DISCONNECTED = 0x00;
const uint8_t LOWSIGNAL    = 0x01;
const uint8_t HIGHSIGNAL   = 0x02;
const int SCRATCHBANK = 1; //The scratch bank to use for storing the signal strength.


/********************************* DECLARE GLOBAL VARS *******************************************/
uint8_t buffer[1];
int state = 0;
AccelerationReading previousAccel;


void setup() {
  // Motor Controller Setup
  pinMode(STBY, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);

  // Switch Setup
  pinMode(SWCH, INPUT);
  digitalWrite(SWCH, HIGH); // Enable internal pullup
  digitalWrite(STBY, LOW); //enable standby

  previousAccel = Bean.getAcceleration(); 
  buffer[0] = DISCONNECTED;
  Bean.setScratchData(SCRATCHBANK, buffer, 1);
  
  Serial.begin(57600);  
  Serial.setTimeout(25);
  Bean.enableWakeOnConnect(true);
}

void loop() {
  /*
  Serial.print("Bean is in state: ");
  Serial.print(state);
  Serial.print("\n");

  ScratchData tmp = Bean.readScratchData(1); 
  Serial.print("ScratchBank: ");
  Serial.print(1);
  Serial.print(" contains: ");
  for(int j=0; j<tmp.length; j++) {
   Serial.print(tmp.data[j], DEC);
   Serial.print(" ");
  }
  Serial.print("\n");
  
  Serial.print("signal is: ");
  Serial.print(getSignalStrength());
  Serial.print("\n\n");
  
  Bean.sleep(10000);
  */
  
  uint8_t signal = 0x00;
  switch(state) {
   case OFF:
     if(Bean.getConnectionState()){
       state = ONCLOSED;
       buffer[0] = LOWSIGNAL;
       Bean.setScratchData(SCRATCHBANK, buffer, 1);
     }
     else { Bean.sleep(0xFFFFFFFF); }
     break;
   case ONCLOSED:
     //check signal strength. If signal is high, change to next state. If signal is disconnected, change to last state.
     signal = getSignalStrength();
     if(signal == DISCONNECTED) { state = OFF; }
     else if(signal == HIGHSIGNAL) { 
       state = ONOPENNOMOVE;
       UnlockTheDoor();
     }
     break;
   case ONOPENNOMOVE:
     //check signal strength. If signal is disconnected or low, go to last state.
     signal = getSignalStrength();
     if(signal == DISCONNECTED || signal == LOWSIGNAL) {
       state = ONCLOSED;
       LockTheDoor();
     }
     //check for movement.  If movement, change to next state.
     if(getDoorMovement()) { state = ONOPENMOVING; }
     break;
   case ONOPENMOVING:
     //check for movement.  If no movement, go to last state.
     if(!getDoorMovement()) { state = ONOPENNOMOVE; }
     break;
  }
}

/********************************** SIGNAL STRENGTH FUNCTIONS *****************************************/
uint8_t getSignalStrength() {
  ScratchData currStrength = Bean.readScratchData(SCRATCHBANK);
  return currStrength.data[0];
}


/********************************** MOVEMENT DETECTION FUNCTIONS **************************************/

//assign value to doorState global var.
bool getDoorMovement() {
  // Get the current acceleration with a conversion of 3.91√ó10-3 g/unit.
  AccelerationReading currentAccel = Bean.getAcceleration();
  int accelDifference = getAccelDifference(previousAccel, currentAccel); 
  // Update previousAccel for the next loop.   
  previousAccel = currentAccel;                                            

  // Check if the Bean has been moved beyond our threshold.
  if(accelDifference > ACCELTHRESHOLD){   
    //door is being moved.
    return true;
  }
  else{
    //door is not being moved.
    return false;
  }
}

// This function calculates the difference between two acceleration readings
int getAccelDifference(AccelerationReading readingOne, AccelerationReading readingTwo) {
  int deltaX = abs(readingTwo.xAxis - readingOne.xAxis);
  int deltaY = abs(readingTwo.yAxis - readingOne.yAxis);
  int deltaZ = abs(readingTwo.zAxis - readingOne.zAxis);
  // Return the magnitude
  return deltaX + deltaY + deltaZ;   
}

/************************************** DOOR LOCK FUNCTIONS ******************************************/

//Unlock the door
void UnlockTheDoor() {
  if(digitalRead(SWCH) == HIGH){
    digitalWrite(STBY, HIGH); //disable standby
    move(255,0);
    while(digitalRead(SWCH) == HIGH);
    delay(LOCK_TIMEOUT_MS);
    move(0,0);
    digitalWrite(STBY, LOW); //enable standby
  }
}

//Lock the door
void LockTheDoor() {
  if(digitalRead(SWCH) == LOW){
    digitalWrite(STBY, HIGH); //disable standby
    move(255,1);
    while(digitalRead(SWCH) == LOW);
    delay(UNLOCK_TIMEOUT_MS);
    move(0,1);
    digitalWrite(STBY, LOW); //enable standby
  }
}

//Power the motor.
void move(int speed, int direction) {
  //Move motor at speed and direction
  //speed: 0 is off, and 255 is full speed
  //direction: 0 clockwise, 1 counter-clockwise

  bool inPin1 = LOW;
  bool inPin2 = HIGH;

  if(direction == 1){
    inPin1 = HIGH;
    inPin2 = LOW;
  }
  digitalWrite(AIN1, inPin1);
  digitalWrite(AIN2, inPin2);
  analogWrite(PWMA, speed);
}


